# CoreMedia - License Manager

This role ensures that the CoreMedia licenses are located on the target system.

## usage

```
coremedia_license_directory: '/etc/coremedia/licenses'

coremedia_license_url: ''

coremedia_license_artifacts: []

coremedia_license_user: nobody
coremedia_license_group: "{{ 'nogroup' if ansible_os_family | lower == 'debian' else 'nobody' }}"
```

## examples

**The user and group are not created and must already exist!**

```
coremedia_license_directory: '/opt/coremedia/licenses'

coremedia_license_url: 'http://cm-licenses.test.io/2007.1'

coremedia_license_artifacts:
  - file: 'cs-license.zip'
    destination: 'cms.zip'
  - file: 'mls-license.zip'
    destination: 'mls.zip'
  - file: 'rls-license.zip'
    destination: 'rls.zip'

coremedia_license_user: coremedia
coremedia_license_group: coremedia
```

## tests

```
$ export ARTEFACT_SERVER=http://192.168.124.1
$ export APPLICATION_VERSION=2007.1
$ tox -e py37-ansible29 -- molecule test
```
